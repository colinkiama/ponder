public class Ponder.Handlers.HitHandler {
    private Soup.Server server;
    private Soup.Message msg;
    private string path;
    private GLib.HashTable? query;
    private Soup.ClientContext client;
    // TODO: Make ALLOWED_ORIGIN value configurable by the environment.
    private const string ALLOWED_ORIGIN = "http://www.test-cors.org";

    public HitHandler (Soup.Server server, Soup.Message msg, string path, GLib.HashTable? query,
        Soup.ClientContext client) {
            this.server = server;
            this.msg = msg;
            this.path = path;
            this.query = query;
            this.client = client;
    }

    public static void handle (Soup.Server server, Soup.Message msg, string path, GLib.HashTable? query,
        Soup.ClientContext client) {
        var handler = new HitHandler (server, msg, path, query, client);
        handler.run ();
    }

    public void run () {
        switch (this.msg.method) {
            case "OPTIONS":
                handle_options_request ();
                break;
            case "POST":
                // Grab hit URL from request body. It should just be a plain
                // string.
                handle_post_request ();
                break;
        }
    }

    private void handle_options_request () {
        // CORS: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
        // https://fetch.spec.whatwg.org/#http-cors-protocol
        this.msg.response_headers.append (Ponder.Constants.ACCESS_CONTROL_ALLOW_ORIGIN, ALLOWED_ORIGIN);
        this.msg.response_headers.append (Ponder.Constants.ACCESS_CONTROL_ALLOW_METHODS, "POST, OPTIONS");
        this.msg.status_code = this.msg.request_headers.get_one ("origin") == ALLOWED_ORIGIN ? 200 : 403;
    }

    /**
     * Grabs hit url from requeset body then adds hit to
     * processing queue
     *
     */
    private void handle_post_request () {
        print ("ALLOWED ORIGIN: %s\n", ALLOWED_ORIGIN);
        this.msg.response_headers.append (Ponder.Constants.ACCESS_CONTROL_ALLOW_ORIGIN, ALLOWED_ORIGIN);
        string hit_url = (string)this.msg.request_body.data;

        string url_match_regex = """(http|ftp|https):\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?"""; //vala-lint=line-length
        if (!Regex.match_simple (url_match_regex, hit_url, 0)) {
            this.msg.status_code = 400;
            return;
        }

        // TODO: Make queue_file_path value configurable by the environment.
        add_hit_to_queue ("queue.txt", hit_url) ;
        this.msg.status_code = 200;
    }

    private void add_hit_to_queue (string queue_file_path, string url) {
        File file = File.new_for_path (queue_file_path);

        try {
            FileOutputStream os = file.append_to (GLib.FileCreateFlags.NONE);
            os.write ("%s\n".printf (url).data);
        } catch (Error e) {
                error (e.message);
        }
    }
}

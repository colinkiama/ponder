namespace Ponder.Constants {
    public const string ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method";
    public const string ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";
    public const string ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    public const string ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    public const string ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    public const string ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    public const string ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
    public const string ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";
}

public class Ponder.Server : Soup.Server {
    public Server () {
        assert (this != null);

        // Links:
        //   http://localhost:8088/*
        this.add_handler ("/hit", Ponder.Handlers.HitHandler.handle);
    }

    public static int main (string[] args) {
        try {
            int port = 8088;

            MainLoop loop = new MainLoop ();

            Ponder.Server server = new Ponder.Server ();
            server.listen_all (port, 0);

            loop.run ();
        } catch (Error e) {
            print ("Error: %s\n", e.message);
        }
        return 0;
    }
}
